import osmnx as ox
import networkx as nx
import json
import warnings
import random
from IPython.display import IFrame

warnings.filterwarnings("ignore", message="numpy.dtype size changed")
warnings.filterwarnings("ignore", message="numpy.ufunc size changed")

ox.config(log_console=True, use_cache=True)

cento = ox.graph_from_address('Via Girolamo Frescobaldi, Cento, Italy', distance=800, network_type='drive')
statsCento = ox.extended_stats(cento, connectivity=True, ecc=True)
edge_colors = ox.get_edge_colors_by_attr(cento, attr='length')
origin=2965020722
destination=2050685409
s = 983479809
t = 254920721
route = nx.shortest_path(cento, origin, destination)
ox.plot_graph(cento, edge_color=edge_colors)
fig, ax = ox.plot_graph_route(cento, route, node_size=50)
ox.add_edge_bearings(cento)
bearing_cento = nx.get_edge_attributes(cento, 'bearing')
degree_per_edge = ox.get_route_edge_attributes(cento, route, attribute='bearing')

#inizio riconoscimento effettivo
target = random.choice(list(cento.nodes()))
adj = {}

#funzione che calcola i nodi adiacenti di un nodo a caso nel grafo dato
def bfs(graph, t):
    queue = [t]
    visited = {}
    for n in graph.nodes():
        visited[n] = False
    #visited[t] = True
    while len(queue) != 0:
        u = queue.pop(len(queue)-1)
        node_adj = []
        for node in nx.neighbors(graph, u):
            if not visited[u]:
                node_adj.append(node)
                queue.append(node)
        visited[u] = True
        if len(node_adj) != 0:
            adj[u] = node_adj

#funzione che restituisce un dict dove le chiavi sono le coppie dei nodi adiacenti e i valori le curve
def calculate_bearing(target_adj, bearing_graph):
    bearing_per_node = {}
    for key, value in bearing_graph.items():
        for k, v in target_adj.items():
            tmp = list(target_adj.values())
            if key[0] == k:
                for val in tmp[0]:
                    if key[1] == val:
                        tpl_node = (k, val)
                        bearing_per_node[tpl_node] = value
            elif key[1] == k:
                for val in tmp[0]:
                    if key[0] == val:
                        tpl_node = (val, k)
                        bearing_per_node[tpl_node] = value
    return bearing_per_node

#funzione che restituisce un dict con chiavi coppie di nodi adiacenti e valori lunghezza dell'arco
#si può anche accorpare alla prima
def calculate_length(target_length, length_graph):
    length_per_edge = {}
    for key, value in length_graph.items():
        for k, v in target_length.items():
            tmp = list(target_length.values())
            if key[0] == k:
                for val in tmp[0]:
                    if key[1] == val:
                        tpl_node = (k, val)
                        length_per_edge[tpl_node] = value
            elif key[1] == k:
                for val in tmp[0]:
                    if key[0] == val:
                        tpl_node = (val, k)
                        length_per_edge[tpl_node] = value
    return length_per_edge

def drawGraph(graph, path):

    map = ox.plot_graph_folium(graph, popup_attribute='name', edge_width=2)
    route_map = ox.plot_route_folium(graph, path)
    fileGraph = 'graph.html'
    fileRoute = 'route.html'
    map.save(fileGraph)
    route_map.save(fileRoute)
    IFrame(fileGraph, width=500, height=500)
    IFrame(fileRoute, width=500, height=500)


def searchPath(graph, bearing):

    if len(nx.get_edge_attributes(graph, 'bearing')) == 0:
        ox.add_edge_bearings(graph)

    degree = nx.get_edge_attributes(graph, 'bearing')
    list_deg = {}

    for k, v in enumerate(bearing):
        for key, value in degree.items():
            if value == v:
                list_deg[key] = value

    return list_deg

def calculate_adj():
    adj = {}
    node_adj = []
    target = random.choice(list(cento.nodes()))
    for node in nx.all_neighbors(cento, target):
        node_adj.append(node)
    adj[target] = node_adj
    return adj


def search(target_node):
    bearing_per_node = {}
    for key, value in bearing_cento.items():
        for k, v in target_node.items():
            tmp = list(target_node.values())
            if key[0] == k:
                for val in tmp[0]:
                    if key[1] == val:
                        tpl_node = (k, val)
                        bearing_per_node[tpl_node] = value
            elif key[1] == k:
                for val in tmp[0]:
                    if key[0] == val:
                        tpl_node = (val, k)
                        bearing_per_node[tpl_node] = value
    print(bearing_per_node)

def search_path(graph, source, target, bearing, length, n):
    queue = [source]
    visited = {}
    l = []
    for nd in graph.nodes():
        visited[nd] = False
    #visited[t] = True
    while not visited[target] and n != 0: #len(queue) != 0:
        u = queue.pop(len(queue)-1)
        #node_adj = []
        tmp = [u]
        l.append(u)
        for deg in bearing:
            for ln in length:
                for node in nx.all_neighbors(graph, u):
                    tmp.append(node)
                    try:
                        a = ox.get_route_edge_attributes(graph, tmp, 'bearing')
                        a = int(a[0])
                        b = ox.get_route_edge_attributes(graph, tmp, 'length')
                        b = int(b[0])
                    except:
                        tmp.pop(len(tmp)-1)
                        continue
                    else:
                        tmp.pop(len(tmp)-1)
                        if a != 0 and b != 0:
                            if deg == a and ln == b:
                                l.append(node)
                                #if len(node_adj) != 0:
                                #adj[u] = node_adj
                                u = node
                                tmp = [u]
                                break
                    #tmp.append(node)
                if not visited[u]:
                    #node_adj.append(node)
                    queue.append(node)

                    #tmp.pop(len(tmp)-1)
                n-=1
                visited[u] = True

    if len(l) != 0:
        if l[0] != source or l[len(l)-1] != target:
            print("nessun percorso trovato")
            return 0
    return l



ciao = searchPath(cento, degree_per_edge)
print(ciao)
#print(json.dumps(statsCento, indent=1))
print(degree_per_edge)
print(cento.nodes())




