import numpy as np
import osmnx as ox
import networkx as nx
import sys


print("About to get the map")
g = ox.graph_from_place(sys.argv[1], network_type='drive')
ox.add_edge_bearings(g)
bearing_g = nx.get_edge_attributes(g, 'bearing')
ox.add_edge_lengths(g)
length_g = nx.get_edge_attributes(g, 'length')
print("Got the map")
#ox.add_edge_lengths(g)
#bg = nx.get_edge_attributes(g, 'bearing')

#arg2 e arg3 sono i rumori o tolleranze su curve e lunghezze rispettivamente

#funzione che calcola i percorsi possibili, se esistono restituisce la lista di nodi del percorso
def search_path(graph, bearing, source, length=[]):
    queue = [source]
    visited = {}
    path = [source]

    for nd in graph.nodes():
        visited[nd] = False

    visited[source] = True

    for deg, ln in zip(bearing, length):
        #for ln in length:
            if len(queue) != 0:
                u = queue.pop(0)
                tmp = [u]

                #for ln in length:
                for node in nx.neighbors(graph, u):
                    tmp.append(node)
                    node_bearing = ox.get_route_edge_attributes(graph, tmp, 'bearing')
                    node_length = ox.get_route_edge_attributes(graph, tmp, 'length')
                    #b = ox.get_route_edge_attributes(graph, tmp, 'length')
                    tmp.pop(len(tmp)-1)
                    if node_bearing[0] >= deg - int(sys.argv[2]) and node_bearing[0] <= deg + int(sys.argv[2]):
                        if node_length[0] >= ln - int(sys.argv[3]) and node_length[0] <= ln + int(sys.argv[3]):
                            if node not in path:
                                path.append(node)
                                if not visited[node]:
                                    queue.append(node)
                                    visited[node] = True
                                    break

    if not nx.is_simple_path(graph, path) or len(path) < len(bearing)+1:
        #print("nessun percorso trovato")
        return 0


    return path

#le due seguenti non vengono utilizzate
#funzione che restituisce un dict dove le chiavi sono le coppie dei nodi adiacenti e i valori le curve
def calculate_bearing(bearing_graph, adj):
    bearing_per_node = {}
    for key, value in bearing_graph.items():
        for k, v in adj.items():
            if key[0] == k:
                for val in v:
                    if key[1] == val:
                        tpl_node = (k, val)
                        bearing_per_node[tpl_node] = value
            elif key[1] == k:
                for val in v:
                    if key[0] == val:
                        tpl_node = (val, k)
                        bearing_per_node[tpl_node] = value
    return bearing_per_node

#funzione che restituisce un dict con chiavi coppie di nodi adiacenti e valori lunghezza dell'arco
#si pu� anche accorpare alla prima
def calculate_length(length_graph, adj):
    length_per_edge = {}
    for key, value in length_graph.items():
        for k, v in adj.items():
            if key[0] == k:
                for val in v:
                    if key[1] == val:
                        tpl_node = (k, val)
                        length_per_edge[tpl_node] = value
            elif key[1] == k:
                for val in v:
                    if key[0] == val:
                        tpl_node = (val, k)
                        length_per_edge[tpl_node] = value
    return length_per_edge

#vengono usate queste
#genera un percorso restituendo una lista di curve
def getRandomPathBearing():
    found = False
    while not found:
        try:

            route = nx.shortest_path(g, node1, node2)
            if len(route) > 1:
                found = True
        except:
            pass
    previous = -1
    path = []
    for item in route:
        if previous == -1:
            previous = item
            continue
        try:
            path.append(bearing_g[(previous, item, 0)])
        except:
            print("Not found")
        previous = item
    return path

#prende i due nodi di partenza della funzione precedente per considerare lo stesso percorso ed estrae una lista di lunghezze
def getRandomPathLength(random1, random2):
    found = False
    while not found:
        try:
            route = nx.shortest_path(g, random1, random2)
            if len(route) > 1 and len(route) < 25: # Setting a maximum of 25 turns
                found = True
        except:
            pass
    previous = -1  
    path = []
    for item in route:
        if previous == -1:
            previous = item
            continue
        try:
            path.append(length_g[(previous, item, 0)])
        except:
            print("Not found")
        previous = item
    return path



fw = open('results_' + sys.argv[1].replace(" ", "_") + '.csv', 'a')
prog = 0
#ciclo che ricerca 5000 percorsi esaminando ogni nodo del grafo
for l in range(5000):
    print(str((prog*100.0)/5000.0) + '%', end='\r')
    prog += 1
    sys.stdout.flush()
    node1 = np.random.choice(g.nodes())
    node2 = np.random.choice(g.nodes())
    bearings = getRandomPathBearing()
    lengths = getRandomPathLength(node1, node2)
    counter = 0
    if len(bearings) > 15 or len(lengths) > 15:
        continue
#path_found = {}
#i = 0
    for n in g.nodes():
        res = search_path(g, bearings, n, lengths) #la funzione di ricerca viene lanciata considerando tutti i nodi del grafo come nodo di partenza
        if res != 0:
            counter += 1
            #ox.plot_graph_route(g, res)

    #fw.write(sys.argv[1].replace(" ", "_") + " Noise: " + str(sys.argv[2]) + " Number of bearings: " + str(len(bearings)) + " Paths found: " + str(counter) + " Number of nodes: " + str(len(g.nodes())) + '\n')
    fw.write(sys.argv[1].replace(" ", "_") + " " + str(sys.argv[2]) + " " + str(sys.argv[3]) + " " + str(len(bearings)) + " " + str(counter) + " " + str(len(g.nodes())) + '\n')
        #path_found[i] = res
        #i+=1
        #ox.plot_graph_route(g, res)
#x = nx.all_simple_paths(cento, res[0], res[len(res)-1])
#bearing_nadj = calculate_bearing(bearing_cento, x)
#res = search(bearing=[198.452, 18.452, 288.841], n=3)
#fig, ax = ox.plot_graph_route(cento, res)
#length_nadj = calculate_length(nadj, length_cento)


